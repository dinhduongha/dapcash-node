# dapcash-node
DapCash node

[Cellframe Wiki](https://wiki.cellframe.net)


## This guide will work on Debian/Ubuntu

### Build from sources:

#### Prerequsites:

To successfully complete of the build, you must have following prerequisites preinstalled (packages are named as in Debian GNU/Linux 10 "buster", please found the corresponding packages for your distribution):

* libjson-c-dev
* libsqlite3-dev
* libmemcached-dev (for libdap-server-core submodule)
* libev-dev (for libdap-server-core submodule)
* libmagic-dev (for libdap-server submodule)
* libcurl4-openssl-dev | libcurl4-nss-dev | libcurl4-gnutls-dev (for libdap-server submodule)
* libldb-dev
* libtalloc-dev
* libtevent-dev

## Build

#### Get all dapcash-node sources
  ```
  git clone https://gitlab.demlabs.net/dapcash/dapcash-node.git
  cd dapcash-node
  git submodule update --init
  cd ../
  ```

The project uses CMake. To generate a Makefile run in the project root directory `git submodule init && git submodule update --remote && cmake -S . -B build && cd ./build && make`.
Or if you don't clone the project yet, run `git clone --recursive <repo>` to clone it with all submodules. Then build the project as a regular CMake project with command `cmake -S . -B build && cd ./build && make`.


#### Build cellframe using Cmake framework
Get into directory with dapcash-node and execute the following commands
  ```
  mkdir build
  cd build
  cmake ../
  make -j$(nproc)
  ```
*-j$(nproc)* nrpoc parameter depends on your machine capacity - number of processor cores.
As a result, you should be able to fine make files in your build folder which will be used by cpack command to create an installation package.

### Build dapcash-node package
Use the following command ```cpack``` from the build directory to create dapcash-node installation package.

##### Install from local package
If everyting went well you should be able to find the following file in your build folder ```dapcash-node-1.2-0-Debian-buster-x86_64.deb``` 

Please use ```dpkg``` command to install it:
  ```
  sudo dpkg -i ./dapcash-node-1.2-0-Debian-buster-x86_64.deb
  ```

In some cases there is a following command required to be executed
  ```
  sudo apt --fix-broken install
  ```

##How to install:
apt-get update
apt-get install dapcash-node


### Debian and Ubuntu

Create file /etc/apt/sources.list.d/demlabs.list with one line below:

deb http://debian.pub.demlabs.net/ stretch main non-free

Then download public signature and install it:

wget https://debian.pub.demlabs.net/demlabskey.asc
apt-key add demlabskey.asc

Then update your apt cache and install the package:

apt-get update
apt-get install dapcash-node

