# DapCash testing network
# General section
[general]
id=0x0000000007654321
name=dapcash-testnet
# Possible values: light, full, archive, master, root
node-role={NODE_TYPE}
#node-alias=addr-%node_addr%
#node-addr=1234::1234::1234::1234
gdb_groups_prefix=dapcash-testnet
# node addr exired time in hours (168h=1w 720h=1m 8760h=1y), by default 720h(1week)
#node-addr=0x10
#node-addr-expired=168
# type of node addr [auto, static, dinamic]
node_addr_type=auto
default_chain=bronze

seed_nodes_hostnames=[0.seed.testnet.dapcash.org,1.seed.testnet.dapcash.org,2.seed.testnet.dapcash.org]
seed_nodes_aliases=[dapcash.testnet.root.0,dapcash.testnet.root.1,dapcash.testnet.root.2]
seed_nodes_addrs=[ffff::0000::0000::0001,ffff::0000::0000::0002,ffff::0000::0000::0003]
seed_nodes_port=[80,80,80]

#[role-master]
#proc_chains=[0x00000001]

#[dag-poa]
#events-sign-cert=mycert

#[dag-pos]
#events-sign-wallet=mywallet

