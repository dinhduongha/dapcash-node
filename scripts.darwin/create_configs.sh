#!/bin/bash
# Global settings
export DAP_PREFIX=/Applications/DapCash.app/Contents/Resources
export DAP_APP_NAME=dapcash-node
export DAP_CHAINS_NAME=dapcash
export DAP_CFG_TPL=/Applications/DapCash.app/Contents/Resources/share/configs/$DAP_APP_NAME.cfg.tpl

# Values
export DAP_DEBUG_MODE=false
export DAP_AUTO_ONLINE=true
export DAP_SERVER_ENABLED=false
export DAP_SERVER_ADDRESS=0.0.0.0
export DAP_SERVER_PORT=8089

# DapCash testnet
export DAP_DAPCASH_TESTNET_ENABLED=true
export DAP_DAPCASH_TESTNET_ROLE=full

./create_configs_from_tpl.sh